from django.conf.urls import url, include
from django.views.generic import TemplateView
from services.urls import router as services_router
from .base.viewset import CustomObtainAuthToken

urlpatterns = [
    url(r'^api/token/', CustomObtainAuthToken.as_view(), name='api-token'),
    url(r'api/', include(services_router.urls)),
    url(r"^$", TemplateView.as_view(template_name="index.html")),
    url(r"^(?:.*)/?$", TemplateView.as_view(template_name="index.html")),
]
