from rest_framework_filters import FilterSet
from .models import Service, Notes


class ServiceFilterSet(FilterSet):

    class Meta:
        model = Service
        fields = {
            'professional_id': ['exact'],
        }


class NotesFilterSet(FilterSet):

    class Meta:
        model = Notes
        fields = {
            'service_id': ['exact'],
        }
