from datetime import date
from django.contrib.auth.models import User
from rest_framework import viewsets,  status
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from schedy.base.viewset import SecurityMixin
from .filters import ServiceFilterSet, NotesFilterSet
from .models import (Service,
                     Place,
                     Client,
                     Notes,)
from .serializers import (UserSerializer,
                          ServiceSerializer,
                          PlaceSerializer,
                          ClientSerializer,
                          NoteSerializer,)


class UserViewSet(SecurityMixin, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request):
        data = request.data

        try:
            user = User(
                username=data['username'],
                email=data['email']
            )
            user.set_password('mudar321')
            user.save()
            serial_user = self.serializer_class(user,
                                                context={'request': request})
            return Response(serial_user.data, status.HTTP_201_CREATED)

        except:
            return Response({'status': 'Bad request'},
                            status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def change_password(self, request, pk=None):

        password = request.data.get('password', None)
        if not password:
            return Response({'status': 'Invalid Request'},
                            status=status.HTTP_400_BAD_REQUEST)

        user = request.user
        if not user:
            return Response({'status': 'Invalid Request'},
                            status=status.HTTP_400_BAD_REQUEST)

        user.set_password(password)
        user.save()
        return Response({'status': 'Success',
                         'message': 'Password changed successfully.'})


class ServiceViewSet(SecurityMixin, viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    filter_class = ServiceFilterSet

    def create(self, request):
        data = request.data

        service = Service()
        user = User.objects.get(pk=data['userCreatedId'])
        professional = User.objects.get(pk=data['professionalId'])
        client = Client.objects.get(pk=data['clientId'])
        service.user_created = user
        service.date_created = date.today()
        service.professional = professional
        service.client = client
        service.description = data['description']
        service.title = data['title']
        service.status = data['status']
        service.save()

        serial_service = self.serializer_class(
            service,
            context={'request': request}
        )
        return Response(serial_service.data, status.HTTP_201_CREATED)


class PlaceViewSet(SecurityMixin, viewsets.ModelViewSet):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer


class ClientViewSet(SecurityMixin, viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class NoteViewSet(SecurityMixin, viewsets.ModelViewSet):
    queryset = Notes.objects.all()
    serializer_class = NoteSerializer
    filter_class = NotesFilterSet

    def create(self, request):
        data = request.data
        user = User.objects.get(pk=int(data.pop('user_created').pop()))
        service = Service.objects.get(pk=int(data.pop('service').pop()))

        note = Notes()
        note.user_created = user
        note.service = service
        note.date_created = date.today()
        note.description = data.get('description')
        note.save()

        serial_note = self.serializer_class(
            note,
            context={'request': request}
        )

        return Response(serial_note.data, status.HTTP_201_CREATED)
