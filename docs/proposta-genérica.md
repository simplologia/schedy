## Requisitos 

 - Cadastro de Serviço
 - Cadastro de cliente
 - Agenda (Consulta de Compromissos)
 - Consultar clientes prospectados que não foram convertidos em clientes ativos
 - Consultar serviços na praça
 - Fluxo de Caixa


## Cenários de uso

1. O Administrador vai cadastrar os usuários do sistema.
2. O Administrador vai configurar os acessos permitidos aos usuários.
3. A Secretária deve cadastrar os clientes.
4. A Secretária deve agendar visitas dos clientes.
5. O Engenheiro deve consultar os serviços de sua responsabilidade
6. O Engenheiro deve consultar a agenda de visitas.
7. O Engenheiro pode filtrar os serviços por intervalo de data de atualização, cliente ou status do serviço.
8. O Engenheiro pode filtrar sua agenda por intervalo de data de visita, cliente ou status do serviço.
9. O Engenheiro e a Secretaria devem cadastrar o andamento dos serviços, informando: data e hora, descrição e status.
10. O Engenheiro e a Secretaria devem consultar o extrato de contas à receber, contas a pagar, com saldo diário e mensal;
10. O Engenheiro e a Secretaria devem cadastrar contas a pagar;
11. O Sistema deverá lançar o contas a receber de acordo com o valor e parcelas de um serviço;
12. O Engenheiro e a Secretaria podem filtrar o extrato de contas à receber e à pagar, por intervalo de dias, cliente e serviço;
13. O Engenheiro poderá consultar a lista de clientes por status(Prospectado, Ativo, Inativo e Em debito).
14. Consultar os serviços que se encontrem com um histórico pendente em um determinado local; 
15. A Secretária ou Engenheiro cadastram serviços;


## Protótipos

Consulta de Serviços:
![Serviços][servicos]

Consulta de Detalhes de um Serviço:
![Detalhes de um Serviço][detalhe_servico]

Agendamento de Visitas:
![Agendamento de Visitas][agendamento]


## Arquitetura

O sistema será desenvolvido como um web site, com restrição de acesso por senha e autorização por perfil de usuário às funcionalidades que o sistema conter.
O sistema será desenvolvido com tecnologias open-source, sem exigir nenhum custo sobre os componentes que utilizarmos para construção. A linguagem adotada para a camada servidor será python e o framework adotado será o django, para camada de visualização utilizaremos javascript, html versão cinco e css versão três. utilizaremos também os recursos de design responsivo para permitir que ele seja consultado facilmente em smartphones.
O sistema será hospedado na cloud, fornecido pela empresa Digital Ocean e sua manutenção será efetuada pela empresa Cnaps.

[agendamento]: img/agendamento.png
[servicos]: img/servicos.png
[detalhe_servico]: img/detalhe_servico.png




