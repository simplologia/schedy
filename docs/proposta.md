## Requisitos 

 - Cadastro de Serviço
 - Cadastro de cliente
 - Agenda (Consulta de Compromissos)
 - Consultar clientes prospectados que não foram convertidos em clientes ativos
 - Consultar serviços na praça
 - Fluxo de Caixa


## Cenários de uso

1. O Administrador vai cadastrar os usuários do sistema.
2. O Administrador vai configurar os acessos permitidos aos usuários.
3. A Secretária deve cadastrar os clientes.
4. A Secretária deve agendar visitas dos clientes.
5. O Engenheiro deve consultar os serviços de sua responsabilidade
6. O Engenheiro deve consultar a agenda de visitas.
7. O Engenheiro pode filtrar os serviços por intervalo de data de atualização, cliente ou status do serviço.
8. O Engenheiro pode filtrar sua agenda por intervalo de data de visita, cliente ou status do serviço.
9. O Engenheiro e a Secretaria devem cadastrar o andamento dos serviços, informando: data e hora, descrição e status.
10. O Engenheiro e a Secretaria devem consultar o extrato de contas à receber, contas a pagar, com saldo diário e mensal;
10. O Engenheiro e a Secretaria devem cadastrar contas a pagar;
11. O Sistema deverá lançar o contas a receber de acordo com o valor e parcelas de um serviço;
12. O Engenheiro e a Secretaria podem filtrar o extrato de contas à receber e à pagar, por intervalo de dias, cliente e serviço;
13. O Engenheiro poderá consultar a lista de clientes por status(Prospectado, Ativo, Inativo e Em debito).
14. Consultar os serviços que se encontrem com um histórico pendente em um determinado local; 
15. A Secretária ou Engenheiro cadastram serviços;


## Protótipos

Consulta de Serviços:
![Serviços][servicos]

Consulta de Detalhes de um Serviço:
![Detalhes de um Serviço][detalhe_servico]

Agendamento de Visitas:
![Agendamento de Visitas][agendamento]


## Arquitetura

O sistema será desenvolvido como um web site, com restrição de acesso por senha e autorização por perfil de usuário às funcionalidades que o sistema conter.
O sistema será desenvolvido com tecnologias open-source, sem exigir nenhum custo sobre os componentes que utilizarmos para construção. A linguagem adotada para a camada servidor será python e o framework adotado será o django, para camada de visualização utilizaremos javascript, html versão cinco e css versão três. utilizaremos também os recursos de design responsivo para permitir que ele seja consultado facilmente em smartphones.
O sistema será hospedado na cloud, fornecido pela empresa Digital Ocean e sua manutenção será efetuada pela empresa Cnaps.



## Estimativa de Desenvolvimento

Organizamos o sistema em dois Módulos, o sistema de Agendamento e Acompanhamento de Serviços e o Sistema Financeiro.
Estimativa por módulo:
 - **Acompanhamento e Agendamento de Serviços**: 120horas
 - **Sistema Financeiro**: 100horas

## Proposta comercial

O processo de trabalho se dará por etapas com duração de vinte dias e a cada etapa faremos uma reunião de priorização das tarefas que se encaixem no período de vinte dias, ao término dos vinte dias haverá uma reunião de apresentação e entrega oficial das funcionalidades priorizadas para a etapa. 

Após a primeira entrega o sistema erá disponível para testes e o cliente poderá registrar problemas/melhorias que encontrar/julgar necessários. As melhorias serão encaixadas nas etapas de trabalho com um custo e os problemas não serão cobrados.

Ao final deste projeto o cliente fará pagamentos de R$ 120,00 mensais para manutenção do sistema, durante o período de uso do mesmo.

Caso o cliente deseje que o sistema disponha de melhorias, estas sugestões serão analisadas, estimadas e será feito uma proposta comercial para seu desenvolvimento, uma vez esta proposta aprovada a sugestão de melhoria será implementada.

O custo estimado da hora do desenvolvedor é de R$ 65,00.

Este projeto está estimado em seis etapas de vinte dias cada;

Oferecemos duas variações desta propostas:

1. Pagamento integral das horas estimadas, totalizando R$ 14.300,00, divididos proporcionalmente entre as etapas do projeto, sendo pagas cinco dias uteis após a reunião de apresentação de cada etapa, sendo este sistema exclusivo da Empresa do Marcos.
2. Pagamento parcial das horas estimadas, totalizando R$ 7.150,00, divididos proporcionalmente entre as etapas do projeto, sendo pagas cinco dias uteis após a reunião de apresentação de cada etapa e desta forma cedendo os direitos de uso e comercialização do sistema descrito nesta proposta, para a Asgard Propagando e Cnaps Desenvolvimento de Sistemas.

## Proposta comercial Aceita

Diante das duas propostas apresentadas pela Cnaps e Asgard o Eng. Marco Aurélio aceitou de pagar R$ 5.800,00 sendo o valor hora de R$ 26,36 pelo valor total do projeto e estará isento do pagamento de mensalidade para seu uso, estando limitado à cinco usuários.

Sendo este valor pago de acordo com o número de horas entregues a cada etapa deste projeto, por exemplo: se em uma etapa concluí um número de funcionalidades que represente 50Horas, mediante aprovação das funcionalidades entregues o Eng. Marco Aurélio pagará o valor correspondente à 50 x R$ 26,36 = R$ 1.318,00.

O valor de cada etapa, após aprovada deve ser paga em até 5 dias uteis, não havendo o pagamento a próxima etapa fica paralisada.


## Ordenação de Entrega:

**De:** 06/03/2017 **a** 20/03/2017
1. O Administrador vai cadastrar os usuários do sistema.
2. A Secretária deve cadastrar os clientes.
3. A Secretária ou Engenheiro cadastram serviços;
4. O Engenheiro deve consultar os serviços de sua responsabilidade

**60 Horas: R$ 1.581,80**

**De:** 20/03/2017 **a** 03/04/2017
1. O Administrador vai configurar os acessos permitidos aos usuários.
2. A Secretária deve agendar visitas dos clientes.
3. O Engenheiro deve consultar a agenda de visitas.
4. O Engenheiro pode filtrar os serviços por intervalo de data de atualização, cliente ou status do serviço.

**60 Horas R$ 1.581,80** 


**De:** 03/04/2017 **a** 17/04/2017
1. O Engenheiro pode filtrar sua agenda por intervalo de data de visita, cliente ou status do serviço.
2. O Engenheiro e a Secretaria devem cadastrar o andamento dos serviços, informando: data e hora, descrição e status.
3. O Engenheiro e a Secretaria devem consultar o extrato de contas à receber, contas a pagar, com saldo diário e mensal;
4. O Engenheiro e a Secretaria devem cadastrar contas a pagar;

**40 Horas R$ 1.054,60**


**De:** 17/04/2017 **a** 01/05/2017
1. O Sistema deverá lançar o contas a receber de acordo com o valor e parcelas de um serviço;
2. O Engenheiro e a Secretaria podem filtrar o extrato de contas à receber e à pagar, por intervalo de dias, cliente e serviço;
3. O Engenheiro poderá consultar a lista de clientes por status(Prospectado, Ativo, Inativo e Em debito).
4. Consultar os serviços que se encontrem com um histórico pendente em um determinado local; 

**60 Horas R$ 1.581,80**


## sugestões novas

1. Criar um alerta por nota de determinado serviço para enviar e-mail para engenheiro, quanto esta nota se mantiver pendente;
1. Informar foto do cliente no cadastro de cliente, não é obrigatório;

marcoapss2004@yahoo.com.br

subir na internet;
criar dominio;


[agendamento]: img/agendamento.png
[servicos]: img/servicos.png
[detalhe_servico]: img/detalhe_servico.png




